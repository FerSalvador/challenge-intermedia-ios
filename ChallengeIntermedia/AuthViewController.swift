//
//  AuthViewController.swift
//  ChallengeIntermedia
//
//  Created by Fernando on 25/02/2021.
//

import UIKit
import FirebaseAuth

class AuthViewController: UIViewController {

    @IBOutlet weak var emailTexField: UITextField!
    @IBOutlet weak var passWordTextField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    @IBAction func singUpButtonAction(_ sender: UIButton) {
        if let email = emailTexField.text, let passWord = passWordTextField.text {
            Auth.auth().createUser(withEmail: email, password: passWord) { (result, error) in
                if let result = result, error == nil {
                    self.performSegue(withIdentifier: "GoHome", sender: self)
                }else{
                    let alertController = UIAlertController(title: "Error", message: "Se ha producido un error", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Aceptr", style: .default))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func logInButtonAction(_ sender: UIButton) {
        if let email = emailTexField.text, let passWord = passWordTextField.text {
            Auth.auth().signIn(withEmail: email, password: passWord) { (result, error) in
                if let result = result, error == nil {
                    self.performSegue(withIdentifier: "GoHome", sender: self)
                }else{
                    let alertController = UIAlertController(title: "Error", message: "Se ha producido un error", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Aceptr", style: .default))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
}


